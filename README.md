# Assets

Prepared in the assets folder are some images that you will need.

We've also prepared a hash table to make it easier for you to convert the `icon_code` that the API returns into an `svg` file name:

```javascript
const weatherIconCodeToIcon = {
	1: 1,
	2: 2,
	3: 2,
	4: 2,
	5: 2,
	6: 2,
	7: 2,
	8: 2,
	9: 2,
	10: 3,
	11: 3,
	12: 3,
	13: 4,
	14: 4,
	15: 4,
	16: 5,
	17: 5,
	18: 5,
	19: 6,
	20: 6,
	21: 6,
	22: 6,
	23: 7,
	24: 8,
	25: 9,
	26: 10,
	27: 11,
	28: 12,
	29: 13,
	30: 14,
	31: 15,
	32: 16,
	33: 16,
	34: 16,
	35: 17,
};
```
